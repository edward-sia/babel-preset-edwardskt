# Usage

### Install `babel-preset-edwardskt`

```npm
npm i -S babel-preset-edwardskt
```

### In your project `.babelrc` file, include following preset:

```json
{
  "presets": ["babel-preset-edwardskt"]
}
```

## What's in package

package.json
```
"babel-plugin-transform-runtime": "^6.23.0",
"babel-polyfill": "^6.26.0",
"babel-preset-es2015": "^6.24.1",
"babel-preset-react": "^6.24.1",
"babel-preset-stage-2": "^6.24.1",
"babel-runtime": "^6.26.0"
```

.babelrc
```javascript
module.exports = {
  presets: [
    require("babel-preset-es2015"),
    require("babel-preset-react"),
    require("babel-preset-stage-2")
  ],
  plugins: [
    require("babel-plugin-transform-runtime")
  ]
}
```